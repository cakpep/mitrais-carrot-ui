import React, { Component } from 'react';
import './Page404.css';

class Page404 extends Component {
    render() {
        return (
            <div className="login-container">
                <h1 className="page-title">Page Not Found</h1>
                <div className="login-content">
                    404
                </div>
            </div>
        );
    }
}

export default Page404;